#!/usr/bin/env python3

import json
import pandas as pd
import glob
import addonfactory_splunk_conf_parser_lib as splunk_config_parser
import os.path

# Convert conf to JSON
def confToJSON(config, outputfile):
    sections_dict = dict()
    for section in config.sections():
        items = config.items(section)
        sections_dict[section] = dict(items)
    with open(outputfile, 'w') as f:
        json.dump(sections_dict, f, indent=4)

# Convert conf to JSON list
def confToJSONList(config, outputfile):
    sections_dict_list = []
    for section in config.sections():
        items = config.items(section)
        sections_dict_list.append(dict(items))
    with open(outputfile, 'w') as f:
        json.dump(sections_dict_list, f, indent=4)

# Convert conf to csv
def confToCSV(config, outputfile):
    sections_dict_list = []
    for section in config.sections():
        items = config.items(section)
        sections_dict_list.append(dict(items))
    sections_json = json.dumps(sections_dict_list, sort_keys=True, indent=4)
    savedsearches_df = pd.read_json(sections_json).set_index('__name__')
    savedsearches_df.to_csv(outputfile, encoding='utf-8')

# Get savedsearches.conf files
savedsearches_files = glob.glob('./input/*/*/savedsearches.conf', recursive=True)

# Iterate over savedsearches.conf files and output
for file in savedsearches_files:
    # Make sure the output directory exists
    output_directory = "./output/{}/{}".format(file.split("/")[-3], file.split("/")[-2])
    os.makedirs(output_directory, exist_ok=True)

    # Load savedsearches.conf
    config = splunk_config_parser.TABConfigParser()
    config.read(file)

    # Convert
    confToJSON(config, "{}/savedsearches.json".format(output_directory))
    confToJSONList(config, "{}/savedsearches-list.json".format(output_directory))
    confToCSV(config, "{}/savedsearches.csv".format(output_directory))
