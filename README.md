# savedsearches.conf-Converter
Simple python script to convert savedsearches.conf to other formats (json, csv). This doesn't really have a real purpose, it was just made to create easily readable files outside of splunk.

## Usage ##
- Install required dependencies with `pip install -r requirements.txt`
- Place your splunk app in the `input` folder (`savedsearches.conf` needs to be located at `input/{{APP}}/*/savedsearches.conf`)
- Run the script `python3 convert.py`
- Conversions are outputted to the `output` folder

## Dependencies
[addonfactory-splunk-conf-parser-lib](https://github.com/splunk/addonfactory-splunk-conf-parser-lib)
